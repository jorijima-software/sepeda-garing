var mongojs             = require('mongojs');

exports.insert = function(name, qty, price, unit, callback){
    var insert       = {};
    insert['name'] = name;
    insert['qty']  = qty;
    insert['price']= price;
    insert['unit'] = unit;
    db.collection('inventory').findOne({
        name: name,
        unit: unit
    }, function(err, reply){
        if(err) return callback(err);
        if(reply){
            db.collection('inventory').update(
                {_id: reply._id},
                {$set: {qty: qty+reply.qty}},
                function(err, reply){
                    if(err) return callback(err);
                    return callback(reply);
                }
            );
        } else {
            db.collection('inventory').insert(insert, function(err, reply){
                if(err) return callback(err);
                return callback(reply);
            });
        }
    });

};


